package com.example.finalproject.database;

import androidx.lifecycle.MutableLiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalproject.models.SettingsModel;

import java.util.List;

@Dao
public interface SettingsEntryDao {
    @Insert
    long insert(SettingsModel entry);

    @Query("SELECT * FROM settingsmodel")
    SettingsModel get();

    @Query("SELECT * FROM settingsmodel WHERE id = :id LIMIT 1")
    SettingsModel  findById(long id);

    @Update
    void update(SettingsModel entry);

    @Delete
    void delete(SettingsModel entry);
}
