package com.example.finalproject.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalproject.models.BudgetEntry;

import java.util.List;

@Dao
public interface BudgetEntriesDao {
    @Insert
    long insert(BudgetEntry entry);

    @Query("SELECT * FROM budgetentry")
    List<BudgetEntry> getAll();

    @Query("SELECT * FROM budgetentry WHERE id = :id LIMIT 1")
    BudgetEntry findById(long id);

    @Update
    void update(BudgetEntry entry);

    @Delete
    void delete(BudgetEntry entry);
}



