package com.example.finalproject.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.finalproject.models.BudgetEntry;
import com.example.finalproject.models.HomeworkEntry;
import com.example.finalproject.models.ReminderModel;
import com.example.finalproject.models.SettingsModel;
import com.example.finalproject.models.ShoppingModel;
import com.example.finalproject.models.TodoModel;

@Database(entities = {TodoModel.class, BudgetEntry.class, ShoppingModel.class, ReminderModel.class, HomeworkEntry.class, SettingsModel.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract TodoEntriesDao getTodoEntriesDao();
    public abstract BudgetEntriesDao getBudgetEntriesDao();
    public abstract ShoppingEntriesDao getShoppingEntriesDao();
    public abstract ReminderEntriesDao getReminderEntriesDao();
    public abstract SettingsEntryDao getSettingEntriesDao();
    public abstract HomeworkEntriesDao getHomeworkEntriesDao();


}
