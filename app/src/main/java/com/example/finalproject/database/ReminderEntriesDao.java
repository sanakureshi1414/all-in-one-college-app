package com.example.finalproject.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalproject.models.ReminderModel;

import java.util.List;

@Dao
public interface ReminderEntriesDao {
    @Insert
    long insert(ReminderModel entry);

    @Query("SELECT * FROM remindermodel")
    List<ReminderModel> getAll();

    @Query("SELECT * FROM remindermodel WHERE id = :id LIMIT 1")
    ReminderModel findById(long id);

    @Update
    void update(ReminderModel entry);

    @Delete
    void delete(ReminderModel entry);
}
