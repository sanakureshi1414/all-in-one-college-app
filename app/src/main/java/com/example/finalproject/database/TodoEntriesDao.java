package com.example.finalproject.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalproject.models.TodoModel;

import java.util.List;

@Dao
public interface TodoEntriesDao {
    @Insert
    long insert(TodoModel entry);

    @Query("SELECT * FROM todomodel")
    List<TodoModel> getAll();

    @Query("SELECT * FROM todomodel WHERE id = :id LIMIT 1")
    TodoModel findById(long id);

    @Update
    void update(TodoModel entry);

    @Delete
    void delete(TodoModel entry);
}
