package com.example.finalproject.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalproject.models.ShoppingModel;

import java.util.List;

@Dao
public interface ShoppingEntriesDao {
    @Insert
    long insert(ShoppingModel entry);

    @Query("SELECT * FROM shoppingmodel")
    List<ShoppingModel> getAll();

    @Query("SELECT * FROM shoppingmodel WHERE id = :id LIMIT 1")
    ShoppingModel findById(long id);

    @Update
    void update(ShoppingModel entry);

    @Delete
    void delete(ShoppingModel entry);
}
