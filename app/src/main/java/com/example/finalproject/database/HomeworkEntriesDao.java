package com.example.finalproject.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.finalproject.models.HomeworkEntry;

import java.util.List;

@Dao
public interface HomeworkEntriesDao {
    @Insert
    long insert(HomeworkEntry entry);

    @Query("SELECT * FROM homeworkentry")
    List<HomeworkEntry> getAll();

    @Update
    void update(HomeworkEntry entry);

    @Delete
    void delete(HomeworkEntry entry);
}
