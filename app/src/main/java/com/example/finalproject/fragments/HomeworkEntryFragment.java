package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.finalproject.R;
import com.example.finalproject.database.HomeworkEntriesDao;
import com.example.finalproject.viewmodels.HomeworkViewModel;

public class HomeworkEntryFragment extends Fragment {
    public HomeworkEntryFragment() {
        super(R.layout.fragment_homework_entry);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        HomeworkViewModel homeworkViewModel = new ViewModelProvider(getActivity()).get(HomeworkViewModel.class);

        homeworkViewModel.getCurrentEntry().observe(getViewLifecycleOwner(), (entry) -> {
            if (entry == null) {
                getActivity().getSupportFragmentManager().popBackStack();
            } else {
                TextView assignment = view.findViewById(R.id.homework_entry_assignment_text);
                TextView course = view.findViewById(R.id.homework_entry_course_text);
                TextView date = view.findViewById(R.id.homework_entry_date_text);
                TextView notes = view.findViewById(R.id.homework_entry_notes_text);

                assignment.setText(entry.assignment);
                course.setText(entry.course);
                date.setText(entry.date);
                notes.setText(entry.notes);
            }
        });

        view.findViewById(R.id.homework_delete_button).setOnClickListener(button -> {
            homeworkViewModel.deleteCurrentHomeworkEntry();
        });

        view.findViewById(R.id.homework_edit_button).setOnClickListener(button -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, CreateOrEditHomeworkEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
    }
}
