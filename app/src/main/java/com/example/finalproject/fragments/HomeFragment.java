package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModelProvider;

import com.example.finalproject.R;


import com.example.finalproject.models.SettingsModel;
import com.example.finalproject.viewmodels.SettingsViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class HomeFragment extends Fragment {

    public HomeFragment() {
        super(R.layout.fragment_home);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton button = getActivity().findViewById(R.id.fab);
        button.hide();

        SettingsViewModel viewModel = new ViewModelProvider(getActivity()).get(SettingsViewModel.class);
        SettingsModel entry = viewModel.getEntry().getValue();

        TextView name = view.findViewById(R.id.home_name_view);
        TextView school = view.findViewById(R.id.home_school_view);

        name.setText(entry.name);
        school.setText(entry.school);
    }
}
