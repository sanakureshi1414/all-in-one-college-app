package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.finalproject.R;
import com.example.finalproject.viewmodels.HomeworkViewModel;
import com.google.android.material.button.MaterialButton;

public class CreateOrEditHomeworkEntryFragment extends Fragment {
    private boolean previouslySavingState = false;
    public CreateOrEditHomeworkEntryFragment() {
        super(R.layout.fragment_create_homework_entry);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        HomeworkViewModel homeworkViewModel = new ViewModelProvider(getActivity()).get(HomeworkViewModel.class);
        homeworkViewModel.getCurrentEntry().observe(getViewLifecycleOwner(), (entry) -> {
            if (entry != null) {
                EditText course = view.findViewById(R.id.homework_course_edit);
                EditText assignment = view.findViewById(R.id.homework_assignment_edit);
                EditText date = view.findViewById(R.id.homework_date_edit);
                EditText notes = view.findViewById(R.id.homework_notes_edit);

                course.setText(entry.course);
                assignment.setText(entry.assignment);
                date.setText(entry.date);
                notes.setText(entry.notes);
            }
        });

        homeworkViewModel.getSaving().observe(getViewLifecycleOwner(), saving -> {
            if (saving && !previouslySavingState) {
                MaterialButton button = view.findViewById(R.id.homework_save_button);
                button.setEnabled(false);
                button.setText("Saving...");
                previouslySavingState = saving;
            } else if (previouslySavingState && !saving) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        view.findViewById(R.id.homework_save_button).setOnClickListener(saveButton -> {
            EditText course = view.findViewById(R.id.homework_course_edit);
            EditText assignment = view.findViewById(R.id.homework_assignment_edit);
            EditText date = view.findViewById(R.id.homework_date_edit);
            EditText notes = view.findViewById(R.id.homework_notes_edit);

            homeworkViewModel.saveHomeworkEntry(course.getText().toString(),
                    assignment.getText().toString(),
                    date.getText().toString(),
                    notes.getText().toString());

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, HomeworkFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });

        view.findViewById(R.id.homework_cancel_button).setOnClickListener(cancelButton -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, HomeworkFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
    }
}
