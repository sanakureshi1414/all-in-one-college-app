package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.finalproject.R;
import com.example.finalproject.viewmodels.BudgetViewModel;

public class BudgetEntryFragment extends Fragment {
    public BudgetEntryFragment() {
        super(R.layout.fragment_budget_entry);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BudgetViewModel budgetViewModel = new ViewModelProvider(getActivity()).get(BudgetViewModel.class);

        budgetViewModel.getCurrentEntry().observe(getViewLifecycleOwner(), (entry) -> {
            if (entry == null) {
                getActivity().getSupportFragmentManager().popBackStack();
            } else {
                TextView dateView = view.findViewById(R.id.budget_entry_date_text);
                TextView nameView = view.findViewById(R.id.budget_entry_name_text);
                TextView amountView = view.findViewById(R.id.budget_entry_amount_text);

                dateView.setText(entry.date);
                nameView.setText(entry.name);
                amountView.setText("" + entry.amount);
            }
        });

        view.findViewById(R.id.budget_entry_delete_button).setOnClickListener(button -> {
            budgetViewModel.deleteCurrentBudgetEntry();
        });

        view.findViewById(R.id.budget_entry_edit_button).setOnClickListener(button -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, CreateOrEditBudgetEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
    }
}
