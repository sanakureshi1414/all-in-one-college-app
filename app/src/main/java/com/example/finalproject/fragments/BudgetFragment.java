package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.R;
import com.example.finalproject.adapters.BudgetAdapter;
import com.example.finalproject.viewmodels.BudgetViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class BudgetFragment extends Fragment {
    public BudgetFragment() { super(R.layout.fragment_budgeting); }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton button = getActivity().findViewById(R.id.fab);
        button.show();

        BudgetViewModel budgetViewModel = new ViewModelProvider(getActivity()).get(BudgetViewModel.class);
        ObservableArrayList budgetEntries = budgetViewModel.getBudgetEntries();

        BudgetAdapter budgetAdapter = new BudgetAdapter(budgetEntries, entry -> {
            budgetViewModel.setCurrentEntry(entry);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, BudgetEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });

        budgetEntries.addOnListChangedCallback(new ObservableList.OnListChangedCallback() {
            @Override
            public void onChanged(ObservableList sender) {
                getActivity().runOnUiThread(() -> {
                    budgetAdapter.notifyDataSetChanged();
                });
            }

            @Override
            public void onItemRangeChanged(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    budgetAdapter.notifyItemRangeChanged(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeInserted(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    budgetAdapter.notifyItemRangeInserted(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeMoved(ObservableList sender, int fromPosition, int toPosition, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    budgetAdapter.notifyItemMoved(fromPosition, toPosition);
                });
            }

            @Override
            public void onItemRangeRemoved(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    budgetAdapter.notifyItemRangeRemoved(positionStart, itemCount);
                });
            }
        });

        RecyclerView budgetRecyclerView = view.findViewById(R.id.budget_recycler_view);
        budgetRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        budgetRecyclerView.setAdapter(budgetAdapter);

        button.setOnClickListener(fab -> {
            budgetViewModel.setCurrentEntry(null);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, CreateOrEditBudgetEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
    }
}
