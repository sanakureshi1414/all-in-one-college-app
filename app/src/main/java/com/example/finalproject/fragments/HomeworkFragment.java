package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.R;
import com.example.finalproject.adapters.HomeworkAdapter;
import com.example.finalproject.viewmodels.HomeworkViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class HomeworkFragment extends Fragment {
    public HomeworkFragment () { super(R.layout.fragment_homework); }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton button = getActivity().findViewById(R.id.fab);
        button.hide();

        HomeworkViewModel homeworkViewModel = new ViewModelProvider(getActivity()).get(HomeworkViewModel.class);
        ObservableArrayList homeworkEntries = homeworkViewModel.getHomeworkEntries();

        HomeworkAdapter homeworkAdapter = new HomeworkAdapter(homeworkEntries, entry -> {
            homeworkViewModel.setCurrentEntry(entry);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, HomeworkEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });

        homeworkEntries.addOnListChangedCallback(new ObservableList.OnListChangedCallback() {
            @Override
            public void onChanged(ObservableList sender) {
                getActivity().runOnUiThread(() -> {
                    homeworkAdapter.notifyDataSetChanged();
                });
            }

            @Override
            public void onItemRangeChanged(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    homeworkAdapter.notifyItemRangeChanged(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeInserted(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    homeworkAdapter.notifyItemRangeInserted(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeMoved(ObservableList sender, int fromPosition, int toPosition, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    homeworkAdapter.notifyItemMoved(fromPosition, toPosition);
                });
            }

            @Override
            public void onItemRangeRemoved(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    homeworkAdapter.notifyItemRangeRemoved(positionStart, itemCount);
                });
            }
        });

        RecyclerView homeworkRecyclerView = view.findViewById(R.id.homework_recycler_view);
        homeworkRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        homeworkRecyclerView.setAdapter(homeworkAdapter);

        view.findViewById(R.id.homework_fab).setOnClickListener(homeworkFab -> {
            homeworkViewModel.setCurrentEntry(null);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, CreateOrEditHomeworkEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
    }
}

