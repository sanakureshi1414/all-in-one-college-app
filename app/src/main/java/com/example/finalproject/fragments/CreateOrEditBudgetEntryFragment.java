package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.finalproject.R;
import com.example.finalproject.viewmodels.BudgetViewModel;
import com.google.android.material.button.MaterialButton;

public class CreateOrEditBudgetEntryFragment extends Fragment {
    private boolean previouslySavingState = false;
    public CreateOrEditBudgetEntryFragment() {
        super(R.layout.fragment_create_budget_entry);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BudgetViewModel budgetViewModel = new ViewModelProvider(getActivity()).get(BudgetViewModel.class);
        budgetViewModel.getCurrentEntry().observe(getViewLifecycleOwner(), (entry) -> {
            if (entry != null) {
                EditText date = view.findViewById(R.id.budget_date_edit_text);
                EditText name = view.findViewById(R.id.budget_name_edit_text);
                EditText amount = view.findViewById(R.id.budget_amount_edit_text);

                date.setText(entry.date);
                name.setText(entry.name);
                amount.setText("" + entry.amount);
            }
        });

        budgetViewModel.getSaving().observe(getViewLifecycleOwner(), saving -> {
            if (saving && !previouslySavingState) {
                MaterialButton button = view.findViewById(R.id.budget_save_button);
                button.setEnabled(false);
                button.setText("Saving...");
                previouslySavingState = saving;
            } else if (previouslySavingState && !saving) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        view.findViewById(R.id.budget_save_button).setOnClickListener(saveButton -> {
            EditText date = view.findViewById(R.id.budget_date_edit_text);
            EditText name = view.findViewById(R.id.budget_name_edit_text);
            EditText amount = view.findViewById(R.id.budget_amount_edit_text);

            budgetViewModel.saveBudgetEntry(name.getText().toString(),
                    Double.parseDouble(amount.getText().toString()),
                    date.getText().toString());

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, BudgetFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });

        view.findViewById(R.id.budget_cancel_button).setOnClickListener(cancelButton -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, BudgetFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
    }
}
