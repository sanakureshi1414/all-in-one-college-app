package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.finalproject.R;
import com.example.finalproject.viewmodels.ShoppingViewModel;
import com.google.android.material.button.MaterialButton;

public class CreateOrEditShoppingEntryFragment extends Fragment {
    private boolean previouslySavingState = false;
    public CreateOrEditShoppingEntryFragment() {
        super(R.layout.fragment_create_checklist_entry);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ShoppingViewModel shoppingViewModel = new ViewModelProvider(getActivity()).get(ShoppingViewModel.class);
        shoppingViewModel.getCurrentEntry().observe(getViewLifecycleOwner(), entry -> {
            if (entry != null) {
                EditText shopping = view.findViewById(R.id.new_task_entry);
                shopping.setText(entry.task);
            }
        });

        shoppingViewModel.getSaving().observe(getViewLifecycleOwner(), saving -> {
            if (saving && !previouslySavingState) {
                MaterialButton button = view.findViewById(R.id.enter_button);
                button.setEnabled(false);
                button.setText("Saving...");
                previouslySavingState = saving;
            } else if (previouslySavingState && !saving) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        view.findViewById(R.id.enter_button).setOnClickListener(enterButton -> {
            EditText editText = view.findViewById(R.id.new_task_entry);
            shoppingViewModel.saveShoppingEntry(editText.getText().toString(), false);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, ChecklistFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });

        view.findViewById(R.id.checklist_cancel_button).setOnClickListener(cancelButton -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, ChecklistFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
    }
}
