package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.finalproject.R;
import com.example.finalproject.viewmodels.SettingsViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class SettingsFragment extends Fragment {
    public SettingsFragment() { super(R.layout.fragment_settings); }

        @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.hide();

        SettingsViewModel viewModel = new ViewModelProvider(getActivity()).get(SettingsViewModel.class);

        view.findViewById(R.id.apply_changes_button).setOnClickListener(button -> {
            EditText name = view.findViewById(R.id.name_text_entry);
            EditText school = view.findViewById(R.id.school_text_entry);

            viewModel.saveEntry(name.getText().toString(), school.getText().toString());

            // take keyboard out of the screen
            name.onEditorAction(EditorInfo.IME_ACTION_DONE);
            school.onEditorAction(EditorInfo.IME_ACTION_DONE);

            // display message to notify user information has been saved
            TextView savedNotification = view.findViewById(R.id.notification_view);
            savedNotification.setText("Saving...\nYour changes have been made");
        });
    }
}






