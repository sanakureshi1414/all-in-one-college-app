package com.example.finalproject.fragments;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableList;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.R;
import com.example.finalproject.adapters.ReminderAdapter;
import com.example.finalproject.adapters.ShoppingAdapter;
import com.example.finalproject.adapters.TodoAdapter;
import com.example.finalproject.viewmodels.ReminderViewModel;
import com.example.finalproject.viewmodels.ShoppingViewModel;
import com.example.finalproject.viewmodels.TodoViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ChecklistFragment extends Fragment {

    public ChecklistFragment() { super(R.layout.fragment_checklist); }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.hide();

        // models
        TodoViewModel todoViewModel = new ViewModelProvider(getActivity()).get(TodoViewModel.class);
        ShoppingViewModel shoppingViewModel = new ViewModelProvider(getActivity()).get(ShoppingViewModel.class);
        ReminderViewModel reminderViewModel = new ViewModelProvider(getActivity()).get(ReminderViewModel.class);

        // observable arrayList for each category
        ObservableArrayList todoEntries = todoViewModel.getTodoEntries();
        ObservableArrayList shoppingEntries = shoppingViewModel.getShoppingEntries();
        ObservableArrayList reminderEntries = reminderViewModel.getReminderEntries();

        // view model entries for recycler view
        TodoAdapter todoAdapter = new TodoAdapter(todoEntries, entry -> {
            todoViewModel.setCurrentTodoEntry(entry);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, CreateOrEditTodoEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
        ShoppingAdapter shoppingAdapter = new ShoppingAdapter(shoppingEntries, entry -> {
            shoppingViewModel.setCurrentShoppingEntry(entry);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, CreateOrEditShoppingEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
        ReminderAdapter reminderAdapter = new ReminderAdapter(reminderEntries, entry -> {
            reminderViewModel.setCurrentReminderEntry(entry);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, CreateOrEditReminderEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });

        // handle all TO-DO entries
        todoEntries.addOnListChangedCallback(new ObservableList.OnListChangedCallback() {
            @Override
            public void onChanged(ObservableList sender) {
                getActivity().runOnUiThread(() -> {
                    todoAdapter.notifyDataSetChanged();
                });
            }

            @Override
            public void onItemRangeChanged(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    todoAdapter.notifyItemRangeChanged(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeInserted(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    todoAdapter.notifyItemRangeInserted(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeMoved(ObservableList sender, int fromPosition, int toPosition, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    todoAdapter.notifyItemMoved(fromPosition, toPosition);
                });
            }

            @Override
            public void onItemRangeRemoved(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    todoAdapter.notifyItemRangeRemoved(positionStart, itemCount);
                });
            }
        });

        RecyclerView toDoRecyclerView = view.findViewById(R.id.todo_recycler_view);
        toDoRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        toDoRecyclerView.setAdapter(todoAdapter);

        view.findViewById(R.id.todo_button).setOnClickListener(button -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, CreateOrEditTodoEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });

        //
        //
        //
        // handle all SHOPPING entries
        shoppingEntries.addOnListChangedCallback(new ObservableList.OnListChangedCallback() {
            @Override
            public void onChanged(ObservableList sender) {
                getActivity().runOnUiThread(() -> {
                    shoppingAdapter.notifyDataSetChanged();
                });
            }

            @Override
            public void onItemRangeChanged(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    shoppingAdapter.notifyItemRangeChanged(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeInserted(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    shoppingAdapter.notifyItemRangeInserted(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeMoved(ObservableList sender, int fromPosition, int toPosition, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    shoppingAdapter.notifyItemMoved(fromPosition, toPosition);
                });
            }

            @Override
            public void onItemRangeRemoved(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    shoppingAdapter.notifyItemRangeRemoved(positionStart, itemCount);
                });
            }
        });

        RecyclerView shoppingRecyclerView = view.findViewById(R.id.shopping_recycler_view);
        shoppingRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        shoppingRecyclerView.setAdapter(shoppingAdapter);

        view.findViewById(R.id.shopping_button).setOnClickListener(button -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, CreateOrEditShoppingEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
        //
        //
        //
        //
        // handle all REMINDER entries
        reminderEntries.addOnListChangedCallback(new ObservableList.OnListChangedCallback() {
            @Override
            public void onChanged(ObservableList sender) {
                getActivity().runOnUiThread(() -> {
                    reminderAdapter.notifyDataSetChanged();
                });
            }

            @Override
            public void onItemRangeChanged(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    reminderAdapter.notifyItemRangeChanged(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeInserted(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    reminderAdapter.notifyItemRangeInserted(positionStart, itemCount);
                });
            }

            @Override
            public void onItemRangeMoved(ObservableList sender, int fromPosition, int toPosition, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    reminderAdapter.notifyItemMoved(fromPosition, toPosition);
                });
            }

            @Override
            public void onItemRangeRemoved(ObservableList sender, int positionStart, int itemCount) {
                getActivity().runOnUiThread(() -> {
                    reminderAdapter.notifyItemRangeRemoved(positionStart, itemCount);
                });
            }
        });

        RecyclerView reminderRecyclerView = view.findViewById(R.id.reminders_recycler_view);
        reminderRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        reminderRecyclerView.setAdapter(reminderAdapter);

        view.findViewById(R.id.reminders_button).setOnClickListener(button -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, CreateOrEditReminderEntryFragment.class, null)
                    .setReorderingAllowed(true)
                    .addToBackStack(null)
                    .commit();
        });
    }
}



