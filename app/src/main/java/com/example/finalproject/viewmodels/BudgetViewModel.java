package com.example.finalproject.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import com.example.finalproject.database.AppDatabase;
import com.example.finalproject.models.BudgetEntry;

import java.util.ArrayList;

public class BudgetViewModel extends AndroidViewModel {
    private AppDatabase database;
    private MutableLiveData<Boolean> saving = new MutableLiveData<>();
    private ObservableArrayList<BudgetEntry> entries = new ObservableArrayList<>();
    private MutableLiveData<BudgetEntry> currentEntry = new MutableLiveData<>();

    public BudgetViewModel(@NonNull Application application) {
        super(application);
        saving.setValue(false);
        database = Room.databaseBuilder(application, AppDatabase.class, "appdp").build();

        new Thread(() -> {
            ArrayList<BudgetEntry> budgetEntries = (ArrayList<BudgetEntry>) database.getBudgetEntriesDao().getAll();
            entries.addAll(budgetEntries);
        }).start();
    }

    public MutableLiveData<Boolean> getSaving() {
        return saving;
    }

    public ObservableArrayList<BudgetEntry> getBudgetEntries() {
        return entries;
    }

    public MutableLiveData<BudgetEntry> getCurrentEntry() {
        return currentEntry;
    }

    public void setCurrentEntry(BudgetEntry entry) {
        this.currentEntry.setValue(entry);
    }

    public void deleteCurrentBudgetEntry() {
        new Thread(() -> {
            database.getBudgetEntriesDao().delete(currentEntry.getValue());
            entries.remove(currentEntry.getValue());
            currentEntry.postValue(null);
        }).start();
    }

    public void saveBudgetEntry(String name, double amount, String date) {
        new Thread(() -> {
            if (currentEntry.getValue() != null) {
                BudgetEntry current = currentEntry.getValue();
                current.date = date;
                current.name = name;
                current.amount = amount;
                database.getBudgetEntriesDao().update(current);
                currentEntry.postValue(current);
                int index = entries.indexOf(current);
                entries.set(index, current);
            } else {
                BudgetEntry newEntry = new BudgetEntry();
                newEntry.name = name;
                newEntry.amount = amount;
                newEntry.date = date;
                newEntry.id = database.getBudgetEntriesDao().insert(newEntry);

                entries.add(newEntry);
            }
            saving.postValue(false);
        }).start();
    }
}
