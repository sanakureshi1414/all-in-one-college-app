package com.example.finalproject.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import com.example.finalproject.database.AppDatabase;
import com.example.finalproject.models.ShoppingModel;
import com.example.finalproject.models.TodoModel;

import java.util.ArrayList;

public class ShoppingViewModel extends AndroidViewModel {
    private AppDatabase database;
    private MutableLiveData<Boolean> saving = new MutableLiveData<>();
    private ObservableArrayList<ShoppingModel> entries = new ObservableArrayList<>();
    private MutableLiveData<ShoppingModel> currentEntry = new MutableLiveData<>();

    public ShoppingViewModel (@NonNull Application application) {
        super(application);
        saving.setValue(false);
        database = Room.databaseBuilder(application, AppDatabase.class, "appdb").build();

        new Thread(() -> {
            ArrayList<ShoppingModel> shoppingEntries = (ArrayList<ShoppingModel>) database.getShoppingEntriesDao().getAll();
            entries.addAll(shoppingEntries);
        }).start();
    }

    public MutableLiveData<Boolean> getSaving() {
        return saving;
    }

    public ObservableArrayList<ShoppingModel> getShoppingEntries() {
        return entries;
    }

    public MutableLiveData<ShoppingModel> getCurrentEntry() {
        return currentEntry;
    }

    public void setCurrentShoppingEntry(ShoppingModel entry) {
        this.currentEntry.setValue(entry);
    }

    public void deleteCurrentShoppingEntry() {
        new Thread(() -> {
            database.getShoppingEntriesDao().delete(currentEntry.getValue());
            entries.remove(currentEntry.getValue());
            currentEntry.postValue(null);
        }).start();
    }

    public void saveShoppingEntry(String task, boolean status) {
        new Thread(() -> {
            if (currentEntry != null) {
                ShoppingModel current = currentEntry.getValue();
                current.task = task;

                database.getShoppingEntriesDao().update(current);
                currentEntry.postValue(current);
                int index = entries.indexOf(current);
                entries.set(index, current);
            } else {
                ShoppingModel newEntry = new ShoppingModel();
                newEntry.task = task;
                newEntry.status = status;
                newEntry.id = database.getShoppingEntriesDao().insert(newEntry);

                entries.add(newEntry);
            }
            saving.postValue(false);
        }).start();
    }
}