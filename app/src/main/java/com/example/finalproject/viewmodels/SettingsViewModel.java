package com.example.finalproject.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import com.example.finalproject.database.AppDatabase;
import com.example.finalproject.models.SettingsModel;

import java.util.ArrayList;

public class SettingsViewModel extends AndroidViewModel {
    private AppDatabase database;
    MutableLiveData<SettingsModel> entry = new MutableLiveData<>();
    MutableLiveData<Boolean> saving = new MutableLiveData<>();

    public SettingsViewModel(@NonNull Application application) {
        super(application);
        saving.setValue(false);
        database = Room.databaseBuilder(application, AppDatabase.class, "appdb").build();

        new Thread(() -> {
            SettingsModel data = database.getSettingEntriesDao().get();
            entry.postValue(data);
        }).start();
    }

    public MutableLiveData<Boolean> getSaving() {
        return saving;
    }

    public MutableLiveData<SettingsModel> getEntry() { return entry; }

    public void setEntry(SettingsModel entry) {
        this.entry.setValue(entry);
    }

    public void saveEntry(String name, String school) {
        new Thread(() -> {
            if (entry.getValue() != null) {
                SettingsModel current = entry.getValue();
                current.name = name;
                current.school = school;

                database.getSettingEntriesDao().update(current);
                entry.postValue(current);
            } else {
                SettingsModel newEntry = new SettingsModel();
                newEntry.name = name;
                newEntry.school = school;
                newEntry.id = database.getSettingEntriesDao().insert(newEntry);

                entry.postValue(newEntry);
            }
            saving.postValue(false);
        }).start();
    }
}
