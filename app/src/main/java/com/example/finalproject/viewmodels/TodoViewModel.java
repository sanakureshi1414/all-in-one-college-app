package com.example.finalproject.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import com.example.finalproject.database.AppDatabase;
import com.example.finalproject.models.ShoppingModel;
import com.example.finalproject.models.TodoModel;

import java.util.ArrayList;

public class TodoViewModel extends AndroidViewModel {
    private AppDatabase database;
    private MutableLiveData<Boolean> saving = new MutableLiveData<>();
    private ObservableArrayList<TodoModel> entries = new ObservableArrayList<>();
    private MutableLiveData<TodoModel> currentEntry = new MutableLiveData<>();

    public TodoViewModel(@NonNull Application application) {
        super(application);
        saving.setValue(false);
        database = Room.databaseBuilder(application, AppDatabase.class, "appdb").build();

        new Thread(() -> {
            ArrayList<TodoModel> todoEntries = (ArrayList<TodoModel>) database.getTodoEntriesDao().getAll();
            entries.addAll(todoEntries);
        }).start();
    }

    public MutableLiveData<Boolean> getSaving() {
        return saving;
    }

    public ObservableArrayList<TodoModel> getTodoEntries() {
        return entries;
    }

    public MutableLiveData<TodoModel> getCurrentEntry() {
        return currentEntry;
    }

    public void setCurrentTodoEntry(TodoModel entry) {
        this.currentEntry.setValue(entry);
    }

    public void deleteCurrentToDoEntry() {
        new Thread(() -> {
            database.getTodoEntriesDao().delete(currentEntry.getValue());
            entries.remove(currentEntry.getValue());
            currentEntry.postValue(null);
        }).start();
    }

    public void saveToDoEntry(String task, boolean status) {
        new Thread(() -> {
            if (currentEntry.getValue() != null) {
                TodoModel current = currentEntry.getValue();
                current.task = task;

                database.getTodoEntriesDao().update(current);
                currentEntry.postValue(current);
                int index = entries.indexOf(current);
                entries.set(index, current);
            } else {
                TodoModel newEntry = new TodoModel();
                newEntry.task = task;
                newEntry.status = status;
                newEntry.id = database.getTodoEntriesDao().insert(newEntry);

                entries.add(newEntry);
            }
            saving.postValue(false);
        }).start();
    }
}
