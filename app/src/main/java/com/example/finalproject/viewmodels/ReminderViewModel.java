package com.example.finalproject.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import com.example.finalproject.database.AppDatabase;
import com.example.finalproject.models.ReminderModel;
import com.example.finalproject.models.ShoppingModel;
import com.example.finalproject.models.TodoModel;

import java.util.ArrayList;

public class ReminderViewModel extends AndroidViewModel {
    private AppDatabase database;
    private MutableLiveData<Boolean> saving = new MutableLiveData<>();
    private ObservableArrayList<ReminderModel> entries = new ObservableArrayList<>();
    private MutableLiveData<ReminderModel> currentEntry = new MutableLiveData<>();

    public ReminderViewModel(@NonNull Application application) {
        super(application);
        saving.setValue(false);
        database = Room.databaseBuilder(application, AppDatabase.class, "appdb").build();

        new Thread(() -> {
            ArrayList<ReminderModel> reminderEntries = (ArrayList<ReminderModel>) database.getReminderEntriesDao().getAll();
            entries.addAll(reminderEntries);
        }).start();
    }

    public MutableLiveData<Boolean> getSaving() {
        return saving;
    }

    public ObservableArrayList<ReminderModel> getReminderEntries() {
        return entries;
    }

    public MutableLiveData<ReminderModel> getCurrentEntry() {
        return currentEntry;
    }

    public void setCurrentReminderEntry(ReminderModel entry) {
        this.currentEntry.setValue(entry);
    }

    public void deleteCurrentReminderEntry() {
        new Thread(() -> {
            database.getReminderEntriesDao().delete(currentEntry.getValue());
            entries.remove(currentEntry.getValue());
            currentEntry.postValue(null);
        }).start();
    }

    public void saveReminderEntry(String task, boolean status) {
        new Thread(() -> {
            if (currentEntry != null) {
                ReminderModel current = currentEntry.getValue();
                current.task = task;

                database.getReminderEntriesDao().update(current);
                currentEntry.postValue(current);
                int index = entries.indexOf(current);
                entries.set(index, current);
            } else {
                ReminderModel newEntry = new ReminderModel();
                newEntry.task = task;
                newEntry.status = status;
                newEntry.id = database.getReminderEntriesDao().insert(newEntry);

                entries.add(newEntry);
            }
            saving.postValue(false);
        }).start();
    }
}

