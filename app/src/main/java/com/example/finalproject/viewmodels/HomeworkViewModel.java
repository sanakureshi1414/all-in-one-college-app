package com.example.finalproject.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Room;

import com.example.finalproject.database.AppDatabase;
import com.example.finalproject.models.HomeworkEntry;

import java.util.ArrayList;

public class HomeworkViewModel extends AndroidViewModel {
    private AppDatabase database;
    private MutableLiveData<Boolean> saving = new MutableLiveData<>();
    private ObservableArrayList<HomeworkEntry> entries = new ObservableArrayList<>();
    private MutableLiveData<HomeworkEntry> currentEntry = new MutableLiveData<>();

    public HomeworkViewModel(@NonNull Application application) {
        super(application);
        saving.setValue(false);
        database = Room.databaseBuilder(application, AppDatabase.class, "appdp").build();

        new Thread(() -> {
            ArrayList<HomeworkEntry> homeworkEntries = (ArrayList<HomeworkEntry>) database.getHomeworkEntriesDao().getAll();
            entries.addAll(homeworkEntries);
        }).start();
    }

    public MutableLiveData<Boolean> getSaving() {
        return saving;
    }

    public ObservableArrayList<HomeworkEntry> getHomeworkEntries() {
        return entries;
    }

    public MutableLiveData<HomeworkEntry> getCurrentEntry() {
        return currentEntry;
    }

    public void setCurrentEntry(HomeworkEntry entry) {
        this.currentEntry.setValue(entry);
    }

    public void deleteCurrentHomeworkEntry() {
        new Thread(() -> {
            database.getHomeworkEntriesDao().delete(currentEntry.getValue());
            entries.remove(currentEntry.getValue());
            currentEntry.postValue(null);
        }).start();
    }

    public void saveHomeworkEntry(String course, String assignment, String date, String notes) {
        new Thread(() -> {
            if (currentEntry.getValue() != null) {
                HomeworkEntry current = currentEntry.getValue();
                current.course = course;
                current.assignment = assignment;
                current.date = date;
                current.notes = notes;

                database.getHomeworkEntriesDao().update(current);
                currentEntry.postValue(current);
                int index = entries.indexOf(current);
                entries.set(index, current);
            } else {
                HomeworkEntry newEntry = new HomeworkEntry();
                newEntry.course = course;
                newEntry.assignment = assignment;
                newEntry.date = date;
                newEntry.notes = notes;
                newEntry.id = database.getHomeworkEntriesDao().insert(newEntry);

                entries.add(newEntry);
            }
            saving.postValue(false);
        }).start();
    }
}
