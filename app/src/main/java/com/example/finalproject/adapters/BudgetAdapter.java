package com.example.finalproject.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.R;
import com.example.finalproject.models.BudgetEntry;

public class BudgetAdapter extends RecyclerView.Adapter<BudgetAdapter.ViewHolder> {
    ObservableArrayList<BudgetEntry> entries;

    public interface OnBudgetEntryClicked {
        void onClick(BudgetEntry entry);
    }

    OnBudgetEntryClicked listener;

    public BudgetAdapter(ObservableArrayList<BudgetEntry> entries, OnBudgetEntryClicked listener) {
        this.entries = entries;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.budgeting_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        TextView dateView = holder.itemView.findViewById(R.id.budget_item_date);
        dateView.setText("" + entries.get(position).date);

        TextView nameView = holder.itemView.findViewById(R.id.budget_item_name);
        nameView.setText(entries.get(position).name);

        TextView amountView = holder.itemView.findViewById(R.id.budget_item_amount);
        amountView.setText("" + entries.get(position).amount);

        holder.itemView.setOnClickListener(view -> {
            listener.onClick(entries.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}