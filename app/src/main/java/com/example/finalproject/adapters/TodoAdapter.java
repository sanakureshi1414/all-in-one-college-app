package com.example.finalproject.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.R;
import com.example.finalproject.models.TodoModel;

public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.ViewHolder> {
    ObservableArrayList<TodoModel> entries;

    public interface OnTodoEntryClicked {
        void onClick(TodoModel entry);
    }

    OnTodoEntryClicked listener;

    public TodoAdapter(ObservableArrayList<TodoModel> entries, OnTodoEntryClicked listener) {
        this.entries = entries;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.todo_checkbox_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoAdapter.ViewHolder holder, int position) {
        CheckBox checkBox = holder.itemView.findViewById(R.id.check_box);
        checkBox.setText(entries.get(position).task);

        holder.itemView.setOnClickListener(view -> {
            listener.onClick(entries.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        CheckBox task;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            task = itemView.findViewById(R.id.check_box);
        }
    }
}
