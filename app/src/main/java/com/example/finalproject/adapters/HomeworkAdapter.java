package com.example.finalproject.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.ObservableArrayList;
import androidx.recyclerview.widget.RecyclerView;

import com.example.finalproject.R;
import com.example.finalproject.models.HomeworkEntry;

public class HomeworkAdapter extends RecyclerView.Adapter<HomeworkAdapter.ViewHolder> {
    ObservableArrayList<HomeworkEntry> entries;

    public interface OnHomeworkEntryClicked {
        void onClick(HomeworkEntry entry);
    }

    OnHomeworkEntryClicked listener;

    public HomeworkAdapter(ObservableArrayList<HomeworkEntry> entries, OnHomeworkEntryClicked listener) {
        this.entries = entries;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.homework_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CheckBox course = holder.itemView.findViewById(R.id.homework_course_view);
        TextView assignment = holder.itemView.findViewById(R.id.homework_assignment_view);
        TextView date = holder.itemView.findViewById(R.id.homework_date_view);

        course.setText(entries.get(position).course);
        assignment.setText(entries.get(position).assignment);
        date.setText(entries.get(position).date);

        holder.itemView.setOnClickListener(view -> {
            listener.onClick(entries.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return entries.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder (@NonNull View itemView) {
            super(itemView);
        }
    }

}
