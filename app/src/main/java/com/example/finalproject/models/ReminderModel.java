package com.example.finalproject.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ReminderModel {
    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo
    public boolean status;

    @ColumnInfo
    public String task;
}
