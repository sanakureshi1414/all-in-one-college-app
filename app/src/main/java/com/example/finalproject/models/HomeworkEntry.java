package com.example.finalproject.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class HomeworkEntry {
    @PrimaryKey(autoGenerate = true)
    public long id;

    @ColumnInfo
    public String course;

    @ColumnInfo
    public String assignment;

    @ColumnInfo
    public String date;

    @ColumnInfo
    public String notes;
}
