package com.example.finalproject.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.example.finalproject.R;
import com.example.finalproject.fragments.BudgetFragment;
import com.example.finalproject.fragments.ChecklistFragment;
import com.example.finalproject.fragments.HomeFragment;
import com.example.finalproject.fragments.HomeworkFragment;
import com.example.finalproject.fragments.SettingsFragment;
import com.example.finalproject.models.SettingsModel;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, SettingsFragment.class, null)
                    .setReorderingAllowed(true)
                    .commit();
        }

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        MaterialToolbar toolbar = findViewById(R.id.top_app_barr);
        NavigationView navigationView = findViewById(R.id.navigation_view);

        toolbar.setNavigationOnClickListener((view) -> {
            drawerLayout.open();
        });

        navigationView.setNavigationItemSelectedListener((menuItem) -> {
            menuItem.setChecked(true);
            drawerLayout.close();

            if (menuItem.getItemId() == R.id.home_item){
                // navigate to home fragment
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, HomeFragment.class, null)
                        .setReorderingAllowed(true)
                        .commit();
            }
            if (menuItem.getItemId() == R.id.budget_item){
                // navigate to budget fragment
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, BudgetFragment.class, null)
                        .setReorderingAllowed(true)
                        .commit();

            }
            if (menuItem.getItemId() == R.id.checklist_item){
                // navigate to checklist fragment
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, ChecklistFragment.class, null)
                        .setReorderingAllowed(true)
                        .commit();
            }

            if (menuItem.getItemId() == R.id.homework_item) {
                // navigate to homework fragment
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, HomeworkFragment.class, null)
                        .setReorderingAllowed(true)
                        .commit();
            }

            if (menuItem.getItemId() == R.id.settings_item){
                // navigate to settings fragment
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, SettingsFragment.class, null)
                        .setReorderingAllowed(true)
                        .commit();
            }
            return true;
        });
    }
}

